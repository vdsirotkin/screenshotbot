package com.vdsirotkin.telegram.screenshotbot.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by vitaly on 20.09.17.
 */
@Repository
public interface TokensStorageRepository extends CrudRepository<TokensStorageEntity, Integer> {
    TokensStorageEntity findByTsUserId(Long tsUserId);

    void deleteByTsUserId(Long tsUserId);
}
