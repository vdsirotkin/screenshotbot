package com.vdsirotkin.telegram.screenshotbot.jpa;

import javax.persistence.*;

/**
 * Created by vitalijsirotkin on 28.02.16.
 */
@Entity
@Table(name = "TOKENS_STORAGE", schema = "screenshot_bot", catalog = "screenshot_bot")
public class TokensStorageEntity {

    public TokensStorageEntity() {
    }

    public TokensStorageEntity(Long tsUserId, String tsUserToken) {
        this.tsUserId = tsUserId;
        this.tsUserToken = tsUserToken;
    }

    private int tsPk;
    private Long tsUserId;
    private String tsUserToken;

    @Id
    @Column(name = "TS_PK")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getTsPk() {
        return tsPk;
    }

    public void setTsPk(int tsPk) {
        this.tsPk = tsPk;
    }

    @Basic
    @Column(name = "TS_USER_ID")
    public Long getTsUserId() {
        return tsUserId;
    }

    public void setTsUserId(Long tsUserId) {
        this.tsUserId = tsUserId;
    }

    @Basic
    @Column(name = "TS_USER_TOKEN")
    public String getTsUserToken() {
        return tsUserToken;
    }

    public void setTsUserToken(String tsUserToken) {
        this.tsUserToken = tsUserToken;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TokensStorageEntity that = (TokensStorageEntity) o;

        if (tsPk != that.tsPk) return false;
        if (tsUserId != null ? !tsUserId.equals(that.tsUserId) : that.tsUserId != null) return false;
        return !(tsUserToken != null ? !tsUserToken.equals(that.tsUserToken) : that.tsUserToken != null);

    }

    @Override
    public int hashCode() {
        int result = tsPk;
        result = 31 * result + (tsUserId != null ? tsUserId.hashCode() : 0);
        result = 31 * result + (tsUserToken != null ? tsUserToken.hashCode() : 0);
        return result;
    }
}
