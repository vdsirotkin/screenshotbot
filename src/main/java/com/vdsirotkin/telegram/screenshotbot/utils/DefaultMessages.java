package com.vdsirotkin.telegram.screenshotbot.utils;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.BotApiMethod;
import org.telegram.telegrambots.api.methods.ParseMode;
import org.telegram.telegrambots.api.methods.send.SendMessage;

import java.util.Map;

/**
 * Created by vitalijsirotkin on 22.10.16.
 */
@Component
public class DefaultMessages {
    public SendMessage getCantFindCommand(String s) {
        return new SendMessage().setChatId(s).setText("Неизвестная команда, выберите нужную из списка команд.");
    }

    public SendMessage getCancelCommand(String s) {
        return new SendMessage().setText("Команда отменена.").setChatId(s);
    }

    public SendMessage getFirstAuthStep(String chatId, String url) {
        return new SendMessage(chatId, String.format("Открой эту [ссылку](%s), нажми кнопку *разрешить* и введи сюда код, который появится после разрешения", url)).enableMarkdown(true);
    }

    public SendMessage getFirstAuthStepError(String chatId) {
        return new SendMessage(chatId, "Неправильный код, попробуй еще раз");
    }

    public SendMessage getSuccessAuth(String chatId) {
        return new SendMessage(chatId, "Вы успешно авторизовались. Теперь можно воспользоваться командой /getimage чтобы получить последний скриншот из твоего твиттера!)");
    }

    public SendMessage getCantAuth(String chatId) {
        return new SendMessage(chatId, "Не удалось авторизоваться, начните снова через команду /auth");
    }

    public SendMessage getCantFindPhoto(String chatId) {
        return new SendMessage(chatId, "Невозможно найти фото, попробуй еще раз.");
    }

    public SendMessage getAlreadyAuth(String chatId) {
        return new SendMessage(chatId, "Вы уже авторизовались, можете переходить к команде /getimage. А чтобы переавторизоваться нажмите /start");
    }
}
