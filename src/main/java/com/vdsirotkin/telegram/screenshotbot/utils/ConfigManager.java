package com.vdsirotkin.telegram.screenshotbot.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Created by vitalijsirotkin on 23.09.17.
 */
@Component
@ConfigurationProperties(prefix = "bot")
public class ConfigManager {

    public String username;
    public String token;

    public String getUsername() {
        return username;
    }

    public ConfigManager setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getToken() {
        return token;
    }

    public ConfigManager setToken(String token) {
        this.token = token;
        return this;
    }
}
