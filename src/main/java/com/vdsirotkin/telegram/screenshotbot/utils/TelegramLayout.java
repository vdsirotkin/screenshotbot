package com.vdsirotkin.telegram.screenshotbot.utils;

import com.google.gson.Gson;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.layout.AbstractStringLayout;
import org.telegram.telegrambots.api.methods.send.SendMessage;

import java.nio.charset.Charset;

/**
 * Created by vitaly on 22.09.17.
 */
public class TelegramLayout extends AbstractStringLayout {

    protected TelegramLayout(Charset charset) {
        super(charset);
    }

    @Override
    public String toSerializable(LogEvent logEvent) {
        SendMessage message = new SendMessage(123L, logEvent.getMessage().getFormattedMessage());
        return new Gson().toJson(message);
    }
}
