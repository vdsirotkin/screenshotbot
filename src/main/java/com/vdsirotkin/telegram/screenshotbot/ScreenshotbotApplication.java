package com.vdsirotkin.telegram.screenshotbot;

import com.vdsirotkin.telegram.screenshotbot.jpa.TokensStorageEntity;
import com.vdsirotkin.telegram.screenshotbot.jpa.TokensStorageRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.ApiContext;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;
import org.telegram.telegrambots.generics.BotSession;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@SpringBootApplication
public class ScreenshotbotApplication {
	private static final Logger log = LogManager.getLogger(ScreenshotbotApplication.class);

	@Autowired
	ScreenShotBot bot;

	@Autowired
	TokensStorageRepository repository;

	private static BotSession session;

	static {
		ApiContextInitializer.init();
	}

	@PostConstruct
	public void startBot() {
		TelegramBotsApi api = new TelegramBotsApi();
		try {
			session = api.registerBot(bot);
		} catch (TelegramApiRequestException e) {
			log.error(e, e);
		}
	}

	@PreDestroy
	public void stopBot() {
		session.stop();
	}

	public static void main(String[] args) {
		SpringApplication.run(ScreenshotbotApplication.class, args);
	}
}
