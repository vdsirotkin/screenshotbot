package com.vdsirotkin.telegram.screenshotbot;

import com.vdsirotkin.telegram.screenshotbot.handlers.Handler;
import com.vdsirotkin.telegram.screenshotbot.handlers.HandlerFactory;
import com.vdsirotkin.telegram.screenshotbot.utils.ConfigManager;
import com.vdsirotkin.telegram.screenshotbot.utils.DefaultMessages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.HashMap;

/**
 * Created by vitalijsirotkin on 20.02.16.
 */
@Component
public class ScreenShotBot extends TelegramLongPollingBot {

    private static final Logger log = LogManager.getLogger(ScreenShotBot.class);

    private HashMap<Long, Handler> userHandlerMap = new HashMap<>();

    final HandlerFactory factory;
    final DefaultMessages dm;
    final ConfigManager cm;

    @Autowired
    public ScreenShotBot(HandlerFactory factory, DefaultMessages dm, ConfigManager cm) {
        this.factory = factory;
        this.dm = dm;
        this.cm = cm;
    }

    public void onUpdateReceived(Update update) {
        String text = update.getMessage().getText();
        Long chatId = update.getMessage().getChatId();
        Handler handler = userHandlerMap.get(chatId);
        if (!text.equals("/cancel")) {
            if (handler == null) {
                Handler newHandler = factory.getHandler(text);
                if (newHandler != null) {
                    try {
                        newHandler.handleMessage(update);
                    } catch (TelegramApiException e) {
                        log.error(e, e);
                    }
                    if (!newHandler.isFinished()) {
                        userHandlerMap.put(chatId, newHandler);
                    }
                } else {
                    try {
                        execute(dm.getCantFindCommand(chatId.toString()));
                    } catch (TelegramApiException e) {
                        log.error(e, e);
                    }
                }
            } else {
                try {
                    handler.handleMessage(update);
                } catch (TelegramApiException e) {
                    log.error(e, e);
                }
                if (handler.isFinished()) {
                    userHandlerMap.remove(chatId);
                }
            }
        } else {
            try {
                userHandlerMap.remove(chatId);
                execute(dm.getCancelCommand(chatId.toString()));
            } catch (TelegramApiException e) {
                log.error(e, e);
            }
        }
    }

    public String getBotUsername() {
        return cm.username;
    }

    @Override
    public String getBotToken() {
        return cm.token;
    }
}
