package com.vdsirotkin.telegram.screenshotbot.handlers;

import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.exceptions.TelegramApiException;

/**
 * Created by vitaly on 20.09.17.
 */
public interface Handler {

    void handleMessage(Update update) throws TelegramApiException;

    boolean isFinished();

}
