package com.vdsirotkin.telegram.screenshotbot.handlers;

import com.google.gson.Gson;
import com.vdsirotkin.telegram.screenshotbot.ScreenShotBot;
import com.vdsirotkin.telegram.screenshotbot.TwitterApiHelper;
import com.vdsirotkin.telegram.screenshotbot.jpa.TokensStorageEntity;
import com.vdsirotkin.telegram.screenshotbot.jpa.TokensStorageRepository;
import com.vdsirotkin.telegram.screenshotbot.utils.DefaultMessages;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import twitter4j.auth.AccessToken;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.charset.Charset;

/**
 * Created by vitalijsirotkin on 20.02.16.
 */
@Component
@Scope("prototype")
public class TwitterAuthHandler implements Handler {

    private int step = 1;
    private boolean isFinished = false;

    final ScreenShotBot bot;
    final DefaultMessages dm;
    final TokensStorageRepository repository;

    public static final Gson GSON = new Gson();

    @Autowired
    public TwitterAuthHandler(ScreenShotBot bot, DefaultMessages dm, TokensStorageRepository repository) {
        this.bot = bot;
        this.dm = dm;
        this.repository = repository;
    }

    @Override
    public void handleMessage(Update update) throws TelegramApiException {
        Long chatIdLong = update.getMessage().getChatId();
        String chatId = chatIdLong.toString();
        String text = update.getMessage().getText();
        switch (step) {
            case 1:
                ++step;
                TokensStorageEntity entity = repository.findByTsUserId(chatIdLong);
                if (entity != null) {
                    bot.execute(dm.getAlreadyAuth(chatId));
                    isFinished = true;
                } else {
                    bot.execute(dm.getFirstAuthStep(chatId, TwitterApiHelper.authFirstStep()));
                }
                break;
            case 2:
                if (text.matches("[0-9]+")) {
                    AccessToken accessToken = TwitterApiHelper.authSecondStep(text);
                    if (accessToken != null) {
                        bot.execute(dm.getSuccessAuth(chatId));
                        repository.save(new TokensStorageEntity(chatIdLong, GSON.toJson(accessToken)));
                    } else {
                        bot.execute(dm.getCantAuth(chatId));
                    }
                } else {
                    bot.execute(dm.getFirstAuthStepError(chatId));
                }
                isFinished = true;
                break;
            default:
        }
    }

    @Override
    public boolean isFinished() {
        return isFinished;
    }
}
