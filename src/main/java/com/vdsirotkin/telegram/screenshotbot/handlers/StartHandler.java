package com.vdsirotkin.telegram.screenshotbot.handlers;

import com.vdsirotkin.telegram.screenshotbot.ScreenShotBot;
import com.vdsirotkin.telegram.screenshotbot.jpa.TokensStorageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.exceptions.TelegramApiException;

/**
 * Created by vitalijsirotkin on 23.09.17.
 */
@Component
@Scope("prototype")
public class StartHandler implements Handler {

    final TokensStorageRepository repository;
    final ScreenShotBot bot;

    @Autowired
    public StartHandler(ScreenShotBot bot, TokensStorageRepository repository) {
        this.bot = bot;
        this.repository = repository;
    }

    @Override
    @Transactional
    public void handleMessage(Update update) throws TelegramApiException {
        Long chatId = update.getMessage().getChatId();
        String text = update.getMessage().getText();
        repository.deleteByTsUserId(chatId);
        bot.execute(new SendMessage(chatId, "Привет! Это бот для скачивания скриншотов с твиттера. Нажми на /auth чтобы начать работу"));
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
