package com.vdsirotkin.telegram.screenshotbot.handlers;

import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

/**
 * Created by vitaly on 20.09.17.
 */
@Component
public abstract class HandlerFactory {

    public Handler getHandler(String text) {
        if (text.contains("/auth")) {
            return twitterAuthHandler();
        } else if (text.contains("/getimage")) {
            return imageHandler();
        } else if (text.contains("/start")) {
            return startHandler();
        }
        return null;
    }

    @Lookup
    public abstract GetImageHandler imageHandler();

    @Lookup
    public abstract TwitterAuthHandler twitterAuthHandler();

    @Lookup
    abstract StartHandler startHandler();

}
