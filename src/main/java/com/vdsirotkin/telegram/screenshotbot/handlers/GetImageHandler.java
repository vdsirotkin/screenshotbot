package com.vdsirotkin.telegram.screenshotbot.handlers;

import com.google.gson.Gson;
import com.vdsirotkin.telegram.screenshotbot.ScreenShotBot;
import com.vdsirotkin.telegram.screenshotbot.TwitterApiHelper;
import com.vdsirotkin.telegram.screenshotbot.jpa.TokensStorageEntity;
import com.vdsirotkin.telegram.screenshotbot.jpa.TokensStorageRepository;
import com.vdsirotkin.telegram.screenshotbot.utils.DefaultMessages;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import twitter4j.auth.AccessToken;

import java.io.InputStream;
import java.util.UUID;

/**
 * Created by vitalijsirotkin on 20.02.16.
 */
@Component
@Scope("prototype")
public class GetImageHandler implements Handler {

    final TokensStorageRepository repository;
    final ScreenShotBot bot;
    final DefaultMessages dm;

    public static final Gson GSON = new Gson();

    @Autowired
    public GetImageHandler(ScreenShotBot bot, TokensStorageRepository repository, DefaultMessages dm) {
        this.bot = bot;
        this.repository = repository;
        this.dm = dm;
    }

    @Override
    public void handleMessage(Update update) throws TelegramApiException {
        Long chatId = update.getMessage().getChatId();
        TokensStorageEntity storageEntity = repository.findByTsUserId(chatId);
        if (storageEntity != null) {
            AccessToken accessToken = GSON.fromJson(storageEntity.getTsUserToken(), AccessToken.class);
            InputStream stream = TwitterApiHelper.getImageFromLastPost(accessToken);
            if (stream != null) {
                SendPhoto photo = new SendPhoto();
                photo.setNewPhoto(UUID.randomUUID().toString(), stream).setChatId(chatId);
                bot.sendPhoto(photo);
                IOUtils.closeQuietly(stream);
            } else {
                bot.execute(dm.getCantFindPhoto(chatId.toString()));
            }
        } else {
            bot.execute(new SendMessage(chatId, "Ты еще не авторизован! Нажми на /auth чтобы авторизоваться"));
        }
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
