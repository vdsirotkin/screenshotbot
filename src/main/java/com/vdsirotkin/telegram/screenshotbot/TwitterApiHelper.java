package com.vdsirotkin.telegram.screenshotbot;

import org.apache.commons.io.IOUtils;
import org.apache.http.*;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import twitter4j.*;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * Created by vitalijsirotkin on 20.02.16.
 */
public class TwitterApiHelper {

    private static final Logger logger = LogManager.getLogger(TwitterApiHelper.class);

    static Twitter twitter = TwitterFactory.getSingleton();
    static RequestToken requestToken;

    static {
        twitter.setOAuthConsumer("rCOZWnGXJHq5orUBp7AY8MLrw", "pXGQInsf1icLeelqlFp8XBn7WFH3FBlUeyNnSU8f9VqrU80Zl3");
    }

    public static String authFirstStep() {
        try {
            requestToken = twitter.getOAuthRequestToken();
            return requestToken.getAuthorizationURL();
        } catch (TwitterException e) {
            logger.error(e, e);
        }
        return null;
    }

    public static AccessToken authSecondStep(String pin) {
        try {
            return twitter.getOAuthAccessToken(requestToken, pin);
        } catch (TwitterException e) {
            logger.error(e, e);
        }
        return null;
    }

    public static long getUserName(AccessToken token) {
        if (token != null) {
            twitter.setOAuthAccessToken(token);
        }
        try {
            return twitter.verifyCredentials().getId();
        } catch (TwitterException e) {
            e.printStackTrace();
        }
        return 0l;
    }

    public static InputStream getImageFromLastPost(AccessToken token) {
        twitter.setOAuthAccessToken(token);
        long user_id = getUserName(null);
        try {
            ResponseList<Status> timeline = twitter.getUserTimeline(user_id);
            Status status = timeline.get(0);
            if (status.getMediaEntities().length > 0) {
                MediaEntity mediaEntity = status.getMediaEntities()[0];
                String imageUrl = mediaEntity.getMediaURL() + ":large";
                org.apache.http.client.HttpClient httpClient = HttpClientBuilder.create().build();
                HttpGet get = new HttpGet(imageUrl);
                org.apache.http.HttpResponse response = httpClient.execute(get);
                return response.getEntity().getContent();
            } else {
                return null;
            }
        } catch (TwitterException | IOException e) {
            logger.error(e, e);
        }

        return null;
    }
}
